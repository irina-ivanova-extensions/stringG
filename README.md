## NB! This is a dead project! It's not supported anymore, so there is no guarantee that it will work on latest versions of browser.

# Short Description
Extension generates the string of specified length or calculates the length of specified string.

Perfect tool for boundary testing!

# Download and Install
* **[Install](https://chrome.google.com/webstore/detail/string-generator/mklinoolelpkognneckhecljdfhehfbc "Install")** last version from Google Store

# Questions and Comments
Any questions or comments are welcome! You can write me an e-mail on [irina.ivanova@protonmail.com](irina.ivanova@protonmail.com) or create an issue here.

# Description
Extension is build for software testers to test HTML input fields.

User specifies the length and the type (number or text) of the string and the extension generates the string of specified length. Or user inserts the string in the second field and the extension calculates the length of it.

For example, if length is 10 and type is number, it gives You the string '1000000009' which You can copy-paste into the field. The first and the last characters are always different from the characters in the middle, which helps to see does field holds the whole string or not.
If the field does not holds the whole string (the last character is missing) - You can copy the string to the second extension's textarea field and calculate the length of it - how many characters it actually holds.

First publication of the extension - May 1, 2013.

# Hot Keys
* `[Enter]` makes calculations
* `[Left Arrow ←]` checks Number type
* `[Right Arrow →]` checks Text type
* `[Up Arrow ↑]` checks/unchecks Space checkbox
* `[Down Arrow ↓]` checks/unchecks Umlaut checkbox (ÄÖÜÕ)
* `[Tabular]` sets focus on bottom textarea to insert the String for length calculation

# Chrome Tip
You can configure hot-keys for extension in the Google Chrome:
* open the extensions tab - `chrome://extensions`
* Link "Configure commands" at the bottom
* Choose an extension and type a shortcut
* Now You can use it completely without a mouse!

# Posts about stringG
* *January 29, 2017* [Profile Page with HTML5 and CSS3](https://ivanova-irina.blogspot.nl/2017/01/profile-page-with-html5-and-css3.html "Profile Page with HTML5 and CSS3")
* *March 1, 2014* [String Generator v3.0](http://ivanova-irina.blogspot.com/2014/03/string-generator-v30.html "String Generator v3.0")
* *January 4, 2014* [String Generator v2.4](http://ivanova-irina.blogspot.com/2014/01/string-generator-v24.html "String Generator v2.4")
* *December 4, 2013* [String Generator v2.3](http://ivanova-irina.blogspot.com/2013/12/string-generator-23.html "String Generator v2.3")
* *November 16, 2013* [String Generator v2.0](http://ivanova-irina.blogspot.com/2013/11/string-generator-20.html "String Generator v2.0")
* *September 14, 2013* [String Generator v1.0](http://ivanova-irina.blogspot.com/2013/09/string-generator-extension-v10.html "String Generator v1.0")
* *July 2, 2013* [String Generator v0.5](http://ivanova-irina.blogspot.com/2013/07/string-generator-extension-v05.html "String Generator v0.5")
* *May 1, 2013* [String Generator v0.1](http://ivanova-irina.blogspot.com/2013/05/string-generator-extension.html "String Generator v0.1")
