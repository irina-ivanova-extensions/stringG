function returnError(errorText) {
	setErrorText(errorText);
	selectInputLength();

	throw "";
}

function clearError() {
	var clearError = document.getElementById("error");
	clearError.innerText = "\n\n";
}

function setErrorText(errorText) {
	var divError = document.getElementById("error");
	divError.innerText = errorText + "\n\n";
}

function returnGeneratedString(generatedString) {
	var textarea = document.getElementById("generatedString");
	textarea.value = textarea.value + generatedString;

	selectGeneratedString();
}

function returnCalculatedLength(calculatedLength) {
	var divCalculatedLength = document.getElementById("calculatedLength");
	divCalculatedLength.innerText = "Length: " + calculatedLength + "\n";
}

function setFocusOnInputLength() {
	document.getElementById("inputLength").focus();
}

function setFocusOnInputString() {
	var inputString = document.getElementById("inputString");
	inputString.focus();
}

function setFocusAfterRadioButton(event) {
	var radioButton = event.target;

	if (radioButton.checked) {
		if (document.getElementById("inputTypeNumber").checked)
			disableUmlautCheckbox();
		else
			enableUmlautCheckbox();

		setFocusOnInputLength();
		selectInputLength();
	} else {
		setFocusOnInputLength();
		selectInputLength();
	}
}

function setFocusAfterSpaceCheckbox(event) {
	var spaceCheckbox = event.target;

	if (spaceCheckbox.checked) {
		setFocusOnInputLength();
		selectInputLength();
	} else {
		setFocusOnInputLength();
		selectInputLength();
	}
}

function setFocusAfterUmlautCheckbox(event) {
	var umlautCheckbox = event.target;

	if (umlautCheckbox.checked) {
		setFocusOnInputLength();
		selectInputLength();
	} else {
		setFocusOnInputLength();
		selectInputLength();
	}
}

function selectInputLength() {
	document.getElementById("inputLength").select();
}

function clearInputLength() {
	var inputLength = document.getElementById("inputLength");
	inputLength.value = "";
}

function checkTextRadiobutton() {
	var textRadiobutton = document.getElementById("inputTypeText");
	textRadiobutton.checked = true;
	setFocusOnInputLength();
	enableUmlautCheckbox();
}

function checkNumberRadiobutton() {
	var numberRadiobutton = document.getElementById("inputTypeNumber");
	numberRadiobutton.checked = true;
	setFocusOnInputLength();
	disableUmlautCheckbox();
}

function checkSpaceCheckbox() {
	var spaceCheckbox = document.getElementById("inputSpace");
	spaceCheckbox.checked = true;
}

function uncheckSpaceCheckbox() {
	var spaceCheckbox = document.getElementById("inputSpace");
	spaceCheckbox.checked = false;
}

function checkUmlautCheckbox() {
	var umlautCheckbox = document.getElementById("inputUmlaut");
	umlautCheckbox.checked = true;
}

function uncheckUmlautCheckbox() {
	var umlautCheckbox = document.getElementById("inputUmlaut");
	umlautCheckbox.checked = false;
}

function enableUmlautCheckbox() {
	document.getElementById("inputUmlaut").disabled = false;
}

function disableUmlautCheckbox() {
	document.getElementById("inputUmlaut").disabled = true;
	document.getElementById("inputUmlaut").checked = false;
}

function selectGeneratedString() {
	document.getElementById("generatedString").select();
}

function clearGeneratedString() {
	var clearTextarea = document.getElementById("generatedString");
	clearTextarea.value = "";
}
