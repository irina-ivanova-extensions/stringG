function generateString() {

	clearGeneratedString();
	clearError();
	setVariablesForStringGeneration();

	if (type === "number")
		generateNumberString();
	else if (type === "text")
		generateTextString();

  returnGeneratedString(generatedString);
}

function generateNumberString() {
	if (length > 2) {
		generatedString = "1";

		generateMiddleContent(false, "0");

		generatedString += "9";
	} else if (length === 2)
		generatedString = "19";
	else
		generatedString = "1";
}

function generateTextString() {
	if (umlaut)
		generateTextStringWithUmplauts();
	else
		generateTextStringWithoutUmlauts();
}

function generateTextStringWithUmplauts() {
	if (length === 1)
		generatedString = "Ä";
	else if (length === 2)
		generatedString = "Äö";
	else if (length === 3)
		generatedString = "Äöü";
	else if (length === 4)
		generatedString = "Äöüõ";
	else if (length === 5)
		generatedString = "ÄöüõZ";
	else if (length > 5) {
		generatedString = "Äöüõ";
		generateMiddleContent(true, "x");
		generatedString += "Z";
	}
}

function generateTextStringWithoutUmlauts() {
	if (length > 2) {
		generatedString = "A";
		generateMiddleContent(false, "x");
		generatedString += "Z";
	} else if (length === 2)
		generatedString = "AZ";
	else
		generatedString = "A";
}

function generateMiddleContent(isUmlaut, middleCharacter) {
	var i;

	if (isUmlaut)
		i = 5;
	else
		i = 2;

	for (i; i < length; i++) {
		if (i % 10 === 0) {
			if (space)
				generatedString += " ";
			else
				generatedString += middleCharacter;
		} else
			generatedString += middleCharacter;
	}
}
