var length;
var type;
var space;
var umlaut;
var generatedString;

function setVariablesForStringGeneration() {
	var inputType = getInputType();
	setType(inputType);

	var inputLength = document.getElementById("inputLength").value;
	setLength(inputLength);

	setSpace();
	setUmlaut();
}

function setLength(inputLength) {
	if (inputLength.indexOf(".") === -1 && inputLength.indexOf(",") === -1 && inputLength > 0)
		length = inputLength;
	else
		returnError("Inserted number should be positive integer");
}

function getInputType() {
	if (document.getElementById("inputTypeNumber").checked)
		return "number";
	else if (document.getElementById("inputTypeText").checked)
		return "text";
}

function setType(inputType) {
	if (inputType === "number" || inputType === "text")
		type = inputType;
	else
		returnError("Inserted type is incorrect");
}

function setSpace() {
	if (document.getElementById("inputSpace").checked)
		space = true;
	else
		space = false;
}

function setUmlaut() {
	if (document.getElementById("inputUmlaut").checked)
		umlaut = true;
	else
		umlaut = false;
}

var string;

function setVariablesForCalculateLength() {
	string = document.getElementById("inputString").value;
}
