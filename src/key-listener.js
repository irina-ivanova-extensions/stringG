if (window.addEventListener)
    window.addEventListener("load", listenKeys, false);
else if (window.attachEvent)
    window.attachEvent("onload", listenKeys);
else
    document.addEventListener("load", listenKeys, false);

function listenKeys(){
	listenInputLength(document.getElementById("inputLength"));
	listenRadioButtons(document.getElementById("inputTypeNumber"), document.getElementById("inputTypeText"));
	listenSpaceCheckbox(document.getElementById("inputSpace"));
	listenUmlautCheckbox(document.getElementById("inputUmlaut"));

	listenInputString(document.getElementById("inputString"));
}

function listenInputLength(inputLength) {
    if (inputLength.addEventListener)
        inputLength.addEventListener("keydown", inputLengthListener, false);
    else if (inputLength.attachEvent)
        inputLength.attachEvent("keydown", inputLengthListener);
}

function listenRadioButtons(inputTypeNumber, inputTypeText) {
	if (inputTypeNumber.addEventListener) {
		inputTypeNumber.addEventListener("change", setFocusAfterRadioButton, false);
		inputTypeText.addEventListener("change", setFocusAfterRadioButton, false);
	} else if (inputTypeNumber.attachEvent)
		inputTypeNumber.attachEvent("change", setFocusAfterRadioButton);
}

function listenSpaceCheckbox(inputSpace) {
	if (inputSpace.addEventListener)
		inputSpace.addEventListener("change", setFocusAfterSpaceCheckbox, false);
	else if (inputSpace.attachEvent)
		inputSpace.attachEvent("change", setFocusAfterSpaceCheckbox);
}

function listenUmlautCheckbox(inputUmlaut) {
	if (inputUmlaut.addEventListener)
		inputUmlaut.addEventListener("change", setFocusAfterUmlautCheckbox, false);
	else if (inputUmlaut.attachEvent)
		inputUmlaut.attachEvent("change", setFocusAfterUmlautCheckbox);
}

function listenInputString(inputString) {
    if(inputString.addEventListener)
        inputString.addEventListener("keydown", calculateLengthListener, false);
    else if(inputString.attachEvent)
        inputString.attachEvent("keydown", calculateLengthListener);
}

var enter=13;
var rightArrow=39;
var leftArrow=37;
var upArrow=38;
var downArrow=40;

function inputLengthListener(e) {
	if (e.keyCode === enter)
		generateString();
	else if (e.keyCode === rightArrow) {
		checkTextRadiobutton();
		selectInputLength();
	} else if (e.keyCode === leftArrow) {
		checkNumberRadiobutton();
		selectInputLength();
	} else if (e.keyCode === upArrow) {
		if (document.getElementById("inputSpace").checked)
			uncheckSpaceCheckbox();
		else
			checkSpaceCheckbox();
	} else if (e.keyCode === downArrow) {
		if (document.getElementById("inputUmlaut").checked && document.getElementById("inputTypeText").checked)
			uncheckUmlautCheckbox();
		else if (document.getElementById("inputTypeText").checked)
			checkUmlautCheckbox();
	}
}

function calculateLengthListener(e) {
	if (e.keyCode === enter)
		calculateLength();
}
